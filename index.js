/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - This function should be able to receive a string.
        - Determine if the input username already exists in our registeredUsers array.
            - If it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            - If it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/

function register(userFullName)
{
    
    let userExists = registeredUsers.indexOf(userFullName);

    if (userExists !== -1) 
    {
        alert('Registration failed. Username already exists!');
    } else 
    {
        alert('Thank you for registering!');
        let newUser = registeredUsers.push(userFullName);
    }

    console.log(registeredUsers);
}
register('MIKKI');

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/

function addFriend(stringInput){
    let userExists = registeredUsers.indexOf(stringInput);
    //console.log(userExists);
    if (userExists !== -1) 
    {
        let newFriend = friendsList.push(stringInput);
        alert('You have added '+stringInput+' as a friend!');
    } else 
    {
        alert('User not found.');
    }
}
addFriend('MIKKI');
addFriend('James Jeffries');



/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.
 
*/

function displayFriends(){
    if (friendsList.length !== 0) {
    friendsList.forEach(function(friends)
            {
                console.log(friends);
            });
    }
    else {
        console.log("You currently have 0 friends. Add one first.");
    }
}
displayFriends();

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.

*/
    function displayRegisteredUsers() {
        if (friendsList.length !== 0) {
            let numberOfFriends = friendsList.length;
            alert("You currently have " + numberOfFriends + " friends.")
        } 
        else {
            alert("You currently have 0 friends.")
        }
    }

    

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.
        - In the browser console, log the friendsList array.

*/

    function deleteLastRegisteredUser() {
        if (friendsList.length !== 0) {
           friendsList.splice(friendsList.length-1);
        } 
        else {
            alert("You currently have 0 friends.")
        }
    }
console.log(friendsList);
